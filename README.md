# StarBerry Task

1. Node Version : 14.18.1
2. NPM Version : 6.14.15
3. React Version : 17.0.2
4. WebPack : 5.38.1 

# Install Dependencies

npm install

# Start the application

npm run start

# Build the application for production

1. npm run build
2. npm run build-run 