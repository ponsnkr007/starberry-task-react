import {
    AUTH_CHECK
} from "../types/index";
import { Toast } from '../../library/toaster'

const initialState = {
    username: 'admin',
    password: 'admin',
    isAuthenticated: false
};

export default function (state = initialState, action) {
    const { type, payload } = action
    switch (type) {
        case AUTH_CHECK: {
            const { username, password } = payload;
            if (state.username === username && password === state.password)
                return {
                    ...state,
                    isAuthenticated: true
                }
            else Toast.error('Invalid Login details')
        }
        default:
            return state;
    }
}
