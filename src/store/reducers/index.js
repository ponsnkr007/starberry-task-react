import { combineReducers } from 'redux';
import auth from './auth';
import property from './property';

const appReducer = combineReducers({
    auth,
    property
})
export default (state, action) => {
    return appReducer(state, action)
};