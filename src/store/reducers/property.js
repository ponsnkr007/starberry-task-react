import {
    GET_PROPERTY_LIST_LOADING,
    GET_PROPERTY_LIST_SUCCESS
} from "../types/index";
const initialState = {
    propertyList: [],
    propertyListLoading: false,
    propertyDetail: {},
    propertyDetailLoading: false
};

export default function (state = initialState, action) {
    const { type, payload } = action
    switch (type) {
        case GET_PROPERTY_LIST_LOADING: {
            return {
                ...state,
                propertyListLoading: true
            };
        }
        case GET_PROPERTY_LIST_SUCCESS: {
            return {
                ...state,
                propertyListLoading: false,
                propertyList: payload
            };
        }
        default:
            return state;
    }
}
