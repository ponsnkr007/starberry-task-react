import HTTP from '../../library/http'
import { Toast } from '../../library/toaster'
import {
    GET_PROPERTY_LIST_LOADING,
    GET_PROPERTY_LIST_SUCCESS
} from '../types/index';



export const getPropertyList = ({ payload }) => async dispatch => {
    try {
        const { limit } = payload
        dispatch({
            type: GET_PROPERTY_LIST_LOADING
        });
        const createRequest = HTTP.get({
            url: `${process.env.API_BASE_URL}/properties?_limit=${limit}`
        })
        const executeRequest = await HTTP.execute(createRequest)
        const { data, status } = executeRequest
        if (status === 200)
            dispatch({
                type: GET_PROPERTY_LIST_SUCCESS,
                payload: data,
            });
        else throw new Error('Invalid Request')
    } catch (err) {
        console.log(err)
        Toast.error('Error while getting property list')
    }
};