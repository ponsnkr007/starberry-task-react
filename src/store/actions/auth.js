import HTTP from '../../library/http'
import {
    AUTH_CHECK
} from '../types/index';



export const login = ({ payload }) => async dispatch => {
    try {
        dispatch({
            type: AUTH_CHECK,
            payload: payload,
        });
    } catch (err) {
        console.log(err, 'err')
    }
};