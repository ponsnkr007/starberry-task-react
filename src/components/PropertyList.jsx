import React, { useEffect } from 'react';
import { Container, Header, Grid, Segment } from 'semantic-ui-react';

import { Redirect, useHistory } from 'react-router-dom';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import {
    getPropertyList
} from '../store/actions/property';
import _ from 'lodash'

const mapStateToProps = (state) => ({
    auth: state.auth,
    property: state.property,
});

const PropertyList = ({
    auth: { isAuthenticated },
    property: { propertyList, propertyListLoading },
    getPropertyList,
}) => {
    if (!isAuthenticated) {
        return <Redirect to="/"></Redirect>
    }
    const history = useHistory()
    const getFirstImageForProperty = (images) => {
        if (Array.isArray(images) && images.length > 0)
            return {
                url: images[0].url,
                title: images[0].name
            }
        else return {
            url: 'https://images.pexels.com/photos/186077/pexels-photo-186077.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
            title: 'Property Image'
        }
    }

    const goToPropertyView = (propertyId) => {
        history.push(`/property-view/${propertyId}`)
    }

    useEffect(() => {
        if (propertyList.length === 0)
            getPropertyList({
                payload: {
                    limit: 10
                }
            })
    }, [propertyList])

    return (
        <Segment basic loading={propertyListLoading} style={{ minHeight: '1000px' }}>
            <Container fluid>
                <Grid columns='equal'>
                    <Grid.Row>
                        {propertyList.map((item, i) => {
                            let image = getFirstImageForProperty(item.Images)
                            return <Grid.Column width="3" key={i} onClick={() => {
                                goToPropertyView(item.id)
                            }}>
                                <Segment basic textAlign="center" vertical clearing>
                                    <img src={image.url} alt={image.title} title={image.title} width="200px" height="150px"></img>
                                    <Header as="h5" color="grey">{item.Title.toUpperCase()}</Header>
                                    <Header as="h5" color="grey">{item.Building_Type} | {item.Property_Type}</Header>
                                    <Header as="h5">{item.Price} $</Header>
                                </Segment>
                            </Grid.Column>
                        })}
                    </Grid.Row>
                </Grid>
            </Container>
        </Segment>
    );
};

PropertyList.propTypes = {
    auth: PropTypes.object.isRequired,
    property: PropTypes.object.isRequired,
    getPropertyList: PropTypes.func.isRequired
};

export default connect(mapStateToProps, {
    getPropertyList
})(PropertyList);