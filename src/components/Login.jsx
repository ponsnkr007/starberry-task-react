import React, { useState } from 'react';
import { Button, Form, Grid, Segment, Header } from 'semantic-ui-react';
import { Redirect, useHistory } from 'react-router-dom';
import { Toast } from '../library/toaster';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { login } from '../store/actions/auth';
const mapStateToProps = (state) => ({
    auth: state.auth,
});
const Login = ({ auth: { isAuthenticated }, login }) => {
    if (isAuthenticated) {
        return <Redirect to="/property-list"></Redirect>
    }
    const history = useHistory()
    const [loginFormData, setLoginFormData] = useState({
        value: {
            username: '',
            password: '',
        },
        error: {
            username: true,
            password: true,
        },
        changed: {
            username: false,
            password: false,
        },
        submitted: false,
        hasError: true,
        loading: false,
    });
    const loginFormDataChange = (e, data) => {
        const error = {
            ...loginFormData.error,
            [data.name]: !data.value ? true : false,
        };
        const errorValues = Object.values(error).filter((errorValue) => errorValue === true);
        setLoginFormData({
            ...loginFormData,
            value: {
                ...loginFormData.value,
                [data.name]: data.value,
            },
            changed: {
                ...loginFormData.changed,
                [data.name]: true,
            },
            error: error,
            hasError: errorValues.length ? true : false,
        });
    };

    const onSubmit = async (e) => {
        setLoginFormData({
            ...loginFormData,
            submitted: true,
            loading: !loginFormData.hasError,
        });
        if (loginFormData.hasError) {
            Toast.error('Please enter required fields');
            return;
        }
        login({
            payload: loginFormData.value,
        }).then(() => {
            setLoginFormData({
                ...loginFormData,
                loading: false
            });
        });
    };

    return (
        <Grid textAlign="center" style={{ height: '102vh' }} verticalAlign="middle">
            <Grid.Column style={{ maxWidth: 450 }}>
                <Form size="large" onSubmit={onSubmit} loading={false}>
                    <Segment>
                        <Header>Login</Header>
                        <Form.Input
                            name="username"
                            type="text"
                            placeholder="Username"
                            value={loginFormData.value.username}
                            onChange={loginFormDataChange}
                            icon="user"
                            iconPosition="left"
                            error={
                                loginFormData.error.username &&
                                (loginFormData.changed.username || loginFormData.submitted)
                            }
                            fluid
                        />
                        <Form.Input
                            name="password"
                            type="password"
                            placeholder="Password"
                            value={loginFormData.value.password}
                            onChange={loginFormDataChange}
                            icon="lock"
                            iconPosition="left"
                            error={
                                loginFormData.error.password &&
                                (loginFormData.changed.password || loginFormData.submitted)
                            }
                            fluid
                        />
                        <Button
                            type="submit"
                            content="Sign In"
                            loading={loginFormData.loading}
                            primary
                            labelPosition='right'
                            icon="sign in alternate"
                            size="large"
                            className="skc-log-btn"
                        ></Button>
                    </Segment>
                </Form>
            </Grid.Column>
        </Grid>
    );
};

Login.propTypes = {
    auth: PropTypes.object.isRequired,
    login: PropTypes.func.isRequired,
};

export default connect(mapStateToProps, { login })(Login);
