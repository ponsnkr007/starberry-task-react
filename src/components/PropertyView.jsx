import React, { useEffect, useState, useRef } from 'react';
import { Container, Header, Grid, Button, Segment, Divider, Icon, List, Card } from 'semantic-ui-react';

import { Redirect, useParams } from 'react-router-dom';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import _ from 'lodash'
import ImageScroller from 'react-image-scroller';
import {
    getPropertyList
} from '../store/actions/property';
import { GoogleMap, LoadScript } from '@react-google-maps/api';
const gMapsApiKey = process.env.GOOGLE_MAPS_API_KEY
const mapStateToProps = (state) => ({
    auth: state.auth,
    property: state.property,
});

const PropertyView = ({
    auth: { isAuthenticated },
    property: { propertyList, propertyListLoading },
    getPropertyList
}) => {
    if (!isAuthenticated) {
        return <Redirect to="/"></Redirect>
    }
    const { id } = useParams()
    const containerStyle = {
        width: '100%',
        height: '150px'
    };
    const ref = useRef(null);
    const [propertyDetails, setPropertyDetails] = useState({})
    const [negotiatorDetails, setNegotiatorDetails] = useState({
        name: '',
        designation: '',
        phone: '',
        email: '',
        image: ''
    })
    const [currentImage, setCurrentImage] = useState({
        url: '',
        title: ''
    })
    const [locationLatLong, setLocationLatLong] = useState({
        lat: 75,
        lng: 75
    })
    const [features, setFeatures] = useState({
        yearBuilt: null,
        parking: null,
        pricePerSq: null,
        brochure: null,
        floor_plan: null
    })
    const getFirstImageForProperty = (images) => {
        if (Array.isArray(images) && images.length > 0)
            return {
                url: images[0].url,
                title: images[0].name
            }
        else return {
            url: 'https://images.pexels.com/photos/186077/pexels-photo-186077.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
            title: 'Property Image'
        }
    }

    const changeImageSlider = (current) => {
        if (current !== null) {
            let image = propertyDetails.Images[current]
            setCurrentImage({
                url: image.url,
                title: image.name
            })
        }
    }

    const getNegotiatorDetail = () => {
        setNegotiatorDetails({
            name: propertyDetails.Negotiator.Name,
            designation: propertyDetails.Negotiator.Designation,
            phone: propertyDetails.Negotiator.Phone,
            email: propertyDetails.Negotiator.Email,
            image: propertyDetails.Negotiator.Image.url
        })
    }

    useEffect(() => {
        let getPropertyDetail = _.find(propertyList, { id })
        setPropertyDetails(getPropertyDetail)
    }, [id, propertyList])

    useEffect(() => {
        if (propertyDetails && Object.keys(propertyDetails).length > 0) {
            let currentImage = getFirstImageForProperty(propertyDetails.Images)
            setCurrentImage(currentImage)
            setLocationLatLong({
                lat: propertyDetails.Latitude,
                lng: propertyDetails.Longitude
            })
            setFeatures({
                yearBuilt: propertyDetails.Year_Built ? propertyDetails.Year_Built : null,
                parking: propertyDetails.Parking ? propertyDetails.Parking : null,
                pricePerSq: propertyDetails.Price_Per_Sqm ? propertyDetails.Price_Per_Sqm : null,
                brochure: propertyDetails.Brochure && propertyDetails.Brochure.length ? propertyDetails.Brochure[0].url : null,
                floor_plan: propertyDetails.Floor_Plans && propertyDetails.Floor_Plans.length ? propertyDetails.Floor_Plans[0].url : null
            })
            getNegotiatorDetail()
        }
    }, [propertyDetails])

    useEffect(() => {
        if (propertyList.length === 0)
            getPropertyList({
                payload: {
                    limit: 10
                }
            })
    }, [propertyList])


    return (
        <Segment basic loading={propertyListLoading} style={{ minHeight: '1000px' }}>
            <Container fluid>
                {propertyDetails && Object.keys(propertyDetails).length > 0 ? <Grid>
                    <Grid.Row>
                        <Grid.Column width="10">
                            <Segment basic>
                                <img src={currentImage.url} title={currentImage.title} width="100%" height="450px"></img>
                            </Segment>
                        </Grid.Column>
                        <Grid.Column width="6">
                            <Segment basic>
                                <Segment basic floated="right">
                                    <Icon name="share alternate"></Icon>
                                    <Icon name="heart outline"></Icon>
                                </Segment>
                                <Grid>
                                    <Grid.Row>
                                        <Grid.Column width="3"><Header as="h3">{propertyDetails.Price} $</Header></Grid.Column>
                                        <Grid.Column width="4"><p>{propertyDetails.Bedrooms} bed | {propertyDetails.Floor_Area} sqm</p></Grid.Column>
                                    </Grid.Row>
                                    <Grid.Row>
                                        <Grid.Column width="16"><Header as="h5" color="grey">{propertyDetails.Title.toUpperCase()}</Header></Grid.Column>
                                    </Grid.Row>
                                    <Grid.Row>
                                        <Grid.Column width="16">
                                            <a target="_blank" href={`mailto:help.properties@testmail.com`}>Please contact us</a>
                                        </Grid.Column>
                                    </Grid.Row>
                                    <Grid.Row>
                                        <Grid.Column width="16"> <Button secondary fluid onClick={() => {
                                            window.open(`mailto:${negotiatorDetails.email}`, "_self")
                                        }}>Contact Agent</Button></Grid.Column>
                                    </Grid.Row>
                                    <Grid.Row>
                                        <Grid.Column width="16">
                                            <Header as="h4" color="grey">FACTS &amp; FEATURES</Header>
                                            <Divider />
                                            <List>{features.yearBuilt ?
                                                <List.Item>
                                                    <Grid>
                                                        <Grid.Row>
                                                            <Grid.Column width="8">
                                                                <Header as="h5">Year Built</Header>
                                                            </Grid.Column>
                                                            <Grid.Column width="8">
                                                                <Header as="h5" color="grey">{features.yearBuilt}</Header>
                                                            </Grid.Column>
                                                        </Grid.Row>
                                                    </Grid>
                                                </List.Item>
                                                : ""}
                                                {features.parking ?
                                                    <List.Item>
                                                        <Grid>
                                                            <Grid.Row>
                                                                <Grid.Column width="8">
                                                                    <Header as="h5">Parking</Header>
                                                                </Grid.Column>
                                                                <Grid.Column width="8">
                                                                    <Header as="h5" color="grey">{features.parking}</Header>
                                                                </Grid.Column>
                                                            </Grid.Row>
                                                        </Grid>
                                                    </List.Item>
                                                    : ""}
                                                {features.pricePerSq ?
                                                    <List.Item>
                                                        <Grid>
                                                            <Grid.Row>
                                                                <Grid.Column width="8">
                                                                    <Header as="h5">Price per sqm</Header>
                                                                </Grid.Column>
                                                                <Grid.Column width="8">
                                                                    <Header as="h5" color="grey">$ {features.pricePerSq}</Header>
                                                                </Grid.Column>
                                                            </Grid.Row>
                                                        </Grid>
                                                    </List.Item>
                                                    : ""}
                                                {features.brochure ?
                                                    <List.Item>
                                                        <Grid>
                                                            <Grid.Row>
                                                                <Grid.Column width="8">
                                                                    <Header as="h5">Brochure</Header>
                                                                </Grid.Column>
                                                                <Grid.Column width="8">
                                                                    <a download href={features.brochure}>Download Brochure</a>
                                                                </Grid.Column>
                                                            </Grid.Row>
                                                        </Grid>
                                                    </List.Item>
                                                    : ""}
                                                {features.floorPlan ?
                                                    <List.Item>
                                                        <Grid>
                                                            <Grid.Row>
                                                                <Grid.Column width="8">
                                                                    <Header as="h5">Floor Plan</Header>
                                                                </Grid.Column>
                                                                <Grid.Column width="8">
                                                                    <a target="_blank" href={features.floorPlan}>View Floor Plan</a>
                                                                </Grid.Column>
                                                            </Grid.Row>
                                                        </Grid>
                                                    </List.Item>
                                                    : ""}
                                            </List>
                                        </Grid.Column>
                                    </Grid.Row>
                                </Grid>
                            </Segment>
                        </Grid.Column>
                    </Grid.Row>
                    <Grid.Row>
                        <Grid.Column width="10">
                            <ImageScroller
                                ref={ref}
                                hideScrollbar={false}
                                scrollWithArrows={true}
                                scrollOnClick={true}
                                onChange={(status) => {
                                    changeImageSlider(status.current && status.current.length ? status.current[0] : null)
                                }}
                            >
                                {propertyDetails.Images.map((item, i) => {
                                    return <img src={item.url} alt={item.name} title={item.name} />
                                })}
                            </ImageScroller>
                        </Grid.Column>
                        <Grid.Column width="6">
                            <Segment basic vertical clearing>
                                <Grid>
                                    <Grid.Row>
                                        <Grid.Column width="16" >
                                            <div dangerouslySetInnerHTML={{ __html: propertyDetails.Description }}></div>
                                        </Grid.Column>
                                    </Grid.Row>
                                    <Grid.Row>
                                        <Grid.Column width="16">
                                            <Card fluid>
                                                <Card.Content>
                                                    <img
                                                        src={negotiatorDetails.image}
                                                        width="60px"
                                                        height="60px"
                                                        style={{ float: 'right' }}
                                                    />
                                                    <Card.Header>{negotiatorDetails.name}</Card.Header>
                                                    <Card.Meta>{negotiatorDetails.designation}</Card.Meta>
                                                    <Card.Description>
                                                        <p>{negotiatorDetails.phone} | <a target="_blank" href={`mailto:${negotiatorDetails.email}`}>{negotiatorDetails.email}</a></p>
                                                    </Card.Description>
                                                </Card.Content>
                                            </Card>
                                        </Grid.Column>
                                    </Grid.Row>
                                    <Grid.Row>
                                        <Grid.Column width="16">
                                            <LoadScript
                                                googleMapsApiKey={gMapsApiKey}
                                            >
                                                <GoogleMap
                                                    mapContainerStyle={containerStyle}
                                                    center={locationLatLong}
                                                    zoom={10}
                                                >
                                                </GoogleMap>
                                            </LoadScript>
                                        </Grid.Column>
                                    </Grid.Row>
                                </Grid>
                            </Segment>
                        </Grid.Column>
                    </Grid.Row>
                </Grid> : ""}

            </Container>
        </Segment>
    );
};

PropertyView.propTypes = {
    auth: PropTypes.object.isRequired,
    property: PropTypes.object.isRequired,
    getPropertyList: PropTypes.func.isRequired
};

export default connect(mapStateToProps, {
    getPropertyList
})(PropertyView);