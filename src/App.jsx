import React, { Fragment } from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import { Toaster } from 'react-hot-toast';
import PropertyList from './components/PropertyList'
import PropertyView from './components/PropertyView'
import Login from './components/Login'
const App = () => {
    return (
        <Fragment>
            <Toaster />
            <Router>
                <Switch>
                    <Route exact path="/" component={Login}></Route>
                    <Route exact path="/property-list" component={PropertyList} />
                    <Route exact path="/property-view/:id" component={PropertyView} />
                    <Route path="*"><h1>Not Found</h1></Route>
                </Switch>
            </Router>
        </Fragment>
    );
};

export default App;
