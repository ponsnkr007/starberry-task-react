import axios from 'axios';
const statusCodes = process.env.HTTP_STATUS_CODES_FROM_SERVER.split(',')
const instance = axios.create({
    baseURL: process.env.API_BASE_URL,
    validateStatus: (status) => {
        return statusCodes.indexOf(status) < 0
    }
})

const commonHeaders = {
    'get': {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
    },
    'post': {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
    },
    'patch': {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
    },
    'delete': {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
    },
    'put': {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
    }
}

export default class HTTP {

    static setAuthToken(token) {
        localStorage.setItem('authToken', token)
        instance.defaults.headers.common['Authorization'] = token;
    }

    static getAuthToken() {
        return localStorage.getItem('authToken') !== "null"
            && localStorage.getItem('authToken') !== "undefined"
            && localStorage.getItem('authToken') !== undefined
            && localStorage.getItem('authToken') !== null
            && localStorage.getItem('authToken')
    }

    static get({
        url,
        headers
    }) {
        const config = {
            headers: {
                ...commonHeaders.get,
                ...(headers ? headers : {})
            }
        }
        return instance.get(url, config)
    }

    static post({
        url,
        headers,
        body
    }) {
        const payload = body && Object.keys(body).length > 0 ? JSON.stringify(body) : JSON.stringify({})
        const config = {
            headers: {
                ...commonHeaders.post,
                ...(headers ? headers : {})
            }
        }
        return instance.post(url, payload, config)
    }

    static uploadFile({
        url,
        headers,
        formData
    }) {
        const config = {
            headers: {
                ...commonHeaders.post,
                ...(headers ? headers : {})
            }
        }
        return instance.post(url, formData, config)
    }

    static put({
        url,
        headers,
        body
    }) {
        const payload = body && Object.keys(body).length > 0 ? JSON.stringify(body) : JSON.stringify({})
        const config = {
            headers: {
                ...commonHeaders.put,
                ...(headers ? headers : {})
            }
        }
        return instance.put(url, payload, config)
    }

    static patch({
        url,
        headers,
        body,
    }) {
        const payload = body && Object.keys(body).length > 0 ? JSON.stringify(body) : JSON.stringify({})
        const config = {
            headers: {
                ...commonHeaders.patch,
                ...(headers ? headers : {})
            }
        }
        return instance.patch(url, payload, config)
    }

    static delete({
        url,
        headers,
    }) {
        try {
            const config = {
                headers: {
                    ...commonHeaders.delete,
                    ...(headers ? headers : {})
                }
            }
            return instance.delete(url, config)
        } catch (error) {
            console.log(error)
        }
    }

    static execute(request) {
        return new Promise((resolve, reject) => {
            request.then(res => {
                resolve(res)
            }).catch(err => {
                resolve(err)
            })
        })
    }
}