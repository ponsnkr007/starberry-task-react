import React from 'react'
import { Message } from 'semantic-ui-react'
import toast from 'react-hot-toast';
/**
 * Library: https://react-hot-toast.com/
 */

const errorToaster = message => {
    toast.remove()
    return toast.custom(
        <Message error>
            <Message.Content>
                {message}
            </Message.Content>
        </Message>,
        {
            position: 'top-center',
            duration: 4000
        })
}
const successToaster = message => {
    toast.remove()
    return toast.custom(
        <Message success>
            <Message.Content>
                {message}
            </Message.Content>
        </Message>,
        {
            position: 'top-center'
        })
}
const warningToaster = message => {
    toast.remove()
    return toast.custom(
        <Message warning>
            <Message.Content>
                {message}
            </Message.Content>
        </Message>,
        {
            position: 'top-center'
        })
}
const infoToaster = message => {
    toast.remove()
    return toast.custom(
        <Message info>
            <Message.Content>
                {message}
            </Message.Content>
        </Message>,
        {
            position: 'top-center'
        })
}
export const Toast = {
    error: errorToaster,
    warning: warningToaster,
    info: infoToaster,
    success: successToaster,
}