const path = require('path')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const Dotenv = require('dotenv-webpack');
module.exports = {
    entry: ['@babel/polyfill', path.join(__dirname, "src", "index.jsx")],
    output: {
        path: path.join(__dirname, '/dist'),
        filename: 'index.bundle.js'
    },
    devServer: {
        port: 3003,
        watchContentBase: true,
        historyApiFallback: true
    },
    resolve: {
        extensions: [".ts", ".js", ".jsx", ".json", '.tsx'],
    },
    module: {
        rules: [
            {
                enforce: "pre",
                test: /\.js$/,
                loader: "source-map-loader",
            },
            {
                test: /\.(js|jsx)$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader'
                }
            },
            {
                test: /\.(scss|css)$/,
                use: [
                    MiniCssExtractPlugin.loader,
                    'css-loader',
                    'sass-loader'
                ]
            },
            {
                test: /\.(jpg|jpeg|png|gif|svg|bmp)$/,
                loader: "file-loader",
                options: {
                    outputPath: 'assets/images',
                    name(resourcePath, resourceQuery) {
                        if (process.env.WEBPACK_DEV_SERVER === 'true') {
                            return '[path][name].[ext]';
                        }
                        return '[sha512:hash:base64:7].[ext]';
                    },
                },
            },
            {
                test: /\.(mp4|mp3)$/,  // videos , audios extra
                loader: "file-loader",
                options: {
                    outputPath: 'assets/others',
                    name(resourcePath, resourceQuery) {
                        if (process.env.WEBPACK_DEV_SERVER === 'true') {
                            return '[path][name].[ext]';
                        }
                        return '[sha512:hash:base64:7].[ext]';
                    },
                },
            },
            {
                test: /\.json$/,
                loader: 'json-loader',
                options: {
                    outputPath: 'assets/others',
                    name(resourcePath, resourceQuery) {
                        if (process.env.WEBPACK_DEV_SERVER === 'true') {
                            return '[path][name].[ext]';
                        }
                        return '[sha512:hash:base64:7].[ext]';
                    },
                },
            },
            {
                test: /\.(eot|ttf|woff|woff2|otf)$/,
                loader: 'file-loader',
                options: {
                    outputPath: 'assets/others',
                    name(resourcePath, resourceQuery) {
                        if (process.env.WEBPACK_DEV_SERVER === 'true') {
                            return '[path][name].[ext]';
                        }
                        return '[sha512:hash:base64:7].[ext]';
                    },
                },
            }
        ]
    },

    plugins: [
        new MiniCssExtractPlugin(),
        new Dotenv({
            path: './.env',
            safe: true
        })
    ]
}